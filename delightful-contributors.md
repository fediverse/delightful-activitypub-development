# delightful contributors

These fine people brought us delight by adding their gems of freedom to the delight project.

> **Note**: This page is maintained by you, contributors. Please create a pull request or issue in our tracker if you contributed list entries and want to be included. [More info here](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors).

## We thank you for your gems of freedom :gem:

- [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary) (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder), fediverse: [@humanetech@mastodon.social](https://mastodon.social/@humanetech))

- [André Menrath](https://codeberg.org/linos)

- [Joseph Quigley](https://quigs.link) (codeberg: [@quigs](https://codeberg.org/quigs), fediverse: [@quigs@hachyderm.io](https://hachyderm.io/@quigs))

- [Seviche CC](https://seviche.cc) (codeberg: [@Sevichecc](https://codeberg.org/Sevichecc), fediverse: [@seviche@kongwoo.icu](https://kongwoo.icu/seviche)
